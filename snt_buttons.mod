<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
   <UiMod name="SNT_BUTTONS" version="2.0.2" date="30/04/2009" >
      <Author name="DonKaban" email="k.shabordin@gmail.com" />
      <Description text="SNT actionbar buttons customizer" />
	  <VersionSettings gameVersion="1.2.1" windowsVersion="1.0" savedVariablesVersion="1.0" />
         <Dependencies>
            <Dependency name="EA_ActionBars" optional="false" forceEnable="true" />
            <Dependency name="EASystem_ActionBarClusterManager" optional="false" forceEnable="true" />
            <Dependency name="EASystem_LayoutEditor" optional="false" forceEnable="true" />
            <Dependency name="LibSlash" optional="false" forceEnable="true" />
         </Dependencies>
      <Files>
            <File name="snt_buttons.lua" />
            <File name="snt_buttons.xml" />
      </Files>
      <SavedVariables>
         <SavedVariable name="SNTBTN_DB" global="false" />
      </SavedVariables>
      <OnInitialize>
		 <CallFunction name="snt_buttons.entry_point" />
	  </OnInitialize>
   </UiMod>
</ModuleFile>
