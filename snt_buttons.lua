-----------------------------------------------------------------------------------------------------------------------
-- (c) SNT DEV [http://snt-dev.wikidot.com][k.shabordin@gmail.com] 
-----------------------------------------------------------------------------------------------------------------------
if (not snt_buttons) then snt_buttons = {} end local snt_buttons = snt_buttons local first_load = true
-----------------------------------------------------------------------------------------------------------------------
local VERSION = "2.0"

local MAX_BUTTONS = 32

local default = 
   {
   VERSION           =  VERSION,
   LABEL_FONT        = "font_clear_small_bold",
   TIMER_FONT        = "font_clear_large_bold",
   TIMER_FONT_BIG    = "font_alert_outline_medium",
   CD_CLASSIC        = false,
   CD_FLAME          = true,
   CD_LOS            = true,
   HIDE_TT           = false,
   BAR               = {{columns = 12,back = false,empty = true, page  = 44,buttons = 12,x = 0 ,y = 0,preset = 3},
                        {columns = 12,back = false,empty = true, page  = 44,buttons = 12,x = 0 ,y = 0,preset = 3},
                        {columns = 12,back = false,empty = true, page  = 44,buttons = 12,x = 0 ,y = 0,preset = 3},
                        {columns = 12,back = false,empty = true, page  = 44,buttons = 12,x = 0 ,y = 0,preset = 3},
                        },
   TOGGLER           = {"center","Root","center",0,0},
   FLASHER           = {"center","Root","center",0,-400},
   FLASH_SIZE        = 150,
   CD_ALPHA          = 0.8,
   CDP_TRASHOLD      = 4,
   CDP_ENABLE        = true,
   CDP_ZOOM          = true,
   CDP_ALPHA         = true,
   CDP_TEXT          = true, 
   CDP_ICON          = true, 
   CDP_SKIN          = 1, 
   GCD_ENABLE        = true,
  }
-----------------------------------------------------------------------------------------------------------------------
snt_buttons.buttons_set    =  -- ny,dim,offset
   {{0,56,4},{64,56,4},{128,56,4},{192,44,10},{256,44,10},{320,44,10},{384,44,10},{448,40,12}, 
   {512,50,7},{576,50,7},{640,50,7},{704,50,7},{768,50,7},{832,50,7},{896,56,4},{960,56,4},}

local WTEMPLATE   = "SNT_BUTTONS_FLASH"
local WANCH       = "SNT_BUTTONS_FLASH_ANCHOR"
local DB
---------------------------------------------------------------------------------------------------------------------
local TextLogAddEntry,ButtonSetTexture,WindowClearAnchors,WindowAddAnchor,WindowSetDimensions,
      WindowGetAnchor,LabelSetFont,CooldownDisplaySetCooldown,GetHotbarCooldown,WindowSetShowing,
      WindowGetShowing,ButtonSetPressedFlag,CreateWindow,RegisterEventHandler,DoesWindowExist,
      WindowGetDimensions = 
      TextLogAddEntry,ButtonSetTexture,WindowClearAnchors,WindowAddAnchor,WindowSetDimensions,
      WindowGetAnchor,LabelSetFont,CooldownDisplaySetCooldown,GetHotbarCooldown,WindowSetShowing,
      WindowGetShowing,ButtonSetPressedFlag,CreateWindow,RegisterEventHandler,DoesWindowExist,
      WindowGetDimensions  
---------------------------------------------------------------------------------------------------------------------
local function chat(msg) TextLogAddEntry("Chat",2,StringToWString(msg)) end
local function alert(msg)
   SystemData.AlertText.VecType = {SystemData.AlertText.Types.ORDER}
   SystemData.AlertText.VecText = {msg}
   AlertTextWindow.DISPLAY_TIME = 1.5
   AlertTextWindow.AddAlert()
end
---------------------------------------------------------------------------------------------------------------------
local function actionbars_setup()
   for ndx = 1,4 do
      ActionBarClusterManager.m_Settings:SetActionBarSetting(ndx,"selector",DB.BAR[ndx].page) 
      if(DB.BAR[ndx].empty) then ActionBarClusterManager.m_Settings:SetActionBarSetting(ndx,"showEmptySlots",46)
      else ActionBarClusterManager.m_Settings:SetActionBarSetting(ndx,"showEmptySlots",45) end
      ActionBarClusterManager.m_Settings:SetActionBarSetting(ndx,"background",DB.BAR[ndx].back)
      ActionBarClusterManager.m_Settings:SetActionBarSetting(ndx,"columns",DB.BAR[ndx].columns) 
      ActionBarClusterManager.m_Settings:SetActionBarSetting(ndx,"caps",false)
      ActionBarClusterManager.m_Settings:SetActionBarSetting(ndx,"buttonXSpacing",DB.BAR[ndx].x)
      ActionBarClusterManager.m_Settings:SetActionBarSetting(ndx,"buttonYSpacing",DB.BAR[ndx].y)
      ActionBarClusterManager.m_Settings:SetActionBarSetting(ndx,"buttonCount",DB.BAR[ndx].buttons)
   end
end
---------------------------------------------------------------------------------------------------------------------
local fq = {} -- flash queue
function snt_buttons.do_pulse(inum,flag,cd,obj)
   local wname = WTEMPLATE.."#"..inum
   if(flag) then
      if(not fq[inum]) then 
         fq[inum] = true
       if(DB.CDP_ICON) then
            local PRESET = snt_buttons.buttons_set[DB.CDP_SKIN+7]
            CreateWindowFromTemplate(wname,WTEMPLATE,"Root") 
            WindowSetDimensions(wname,DB.FLASH_SIZE,DB.FLASH_SIZE)
            WindowAddAnchor(wname,unpack(DB.FLASHER))
            local tex,x,y = GetIconData (inum)         
            DynamicImageSetTextureDimensions(wname.."icon",64,64)
            DynamicImageSetTextureDimensions(wname.."border",64,64)
            DynamicImageSetTexture(wname.."icon",tex,0,0)
            DynamicImageSetTexture(wname.."border","buttons",0,PRESET[1])
            if(DB.CDP_ALPHA) then WindowSetAlpha(wname,0) end
      end
         if(DB.CDP_TEXT) then alert(GetAbilityName(obj.m_ActionId)..L" READY!") end
      else
         if(DB.CDP_ICON) then
            if(DB.CDP_ALPHA) then WindowSetAlpha(wname,1-cd) end
            if(DB.CDP_ZOOM)  then WindowSetScale(wname,(1-cd)*1.5) end 
         end
      end
   else
      fq[inum] = nil
      if(DoesWindowExist(wname)) then DestroyWindow(wname) end
   end
end   
---------------------------------------------------------------------------------------------------------------------
local frames  = 
   {
   "ActionIcon","ActionCooldown","ActionCooldownTimer",
   "OverlayFlash","OverlayActive","OverlayGlow"
   }
function snt_buttons.texture_buttons()
   local bar_num = 1
   for _,bar in pairs(ActionBars.m_Bars) do
      local SET = snt_buttons.buttons_set[DB.BAR[bar_num].preset]
      for _,btn in ipairs(bar.m_Buttons) do
         ButtonSetTexture(btn.m_Name.."Action", Button.ButtonState.NORMAL, "buttons",0,SET[1])
         ButtonSetTexture(btn.m_Name.."Action", Button.ButtonState.HIGHLIGHTED, "buttons",64,SET[1])
         ButtonSetTexture(btn.m_Name.."Action", Button.ButtonState.PRESSED, "buttons",128,SET[1])
         ButtonSetTexture(btn.m_Name.."Action", Button.ButtonState.PRESSED_HIGHLIGHTED,"buttons",192,SET[1])
         for ndx = 1,#frames do
            WindowClearAnchors(btn.m_Name..frames[ndx])
            WindowSetDimensions(btn.m_Name..frames[ndx],SET[2],SET[2])
            WindowAddAnchor(btn.m_Name..frames[ndx],"topleft",btn.m_Name.."Action","topleft",SET[3],SET[3])
         end
      end
      if(bar_num < 4) then bar_num = bar_num+1 end
   end
end
-----------------------------------------------------------------------------------------------------------------------
function snt_buttons.save_position(id)
   if(id == LayoutEditor.EDITING_END) then
      DB.TOGGLER[3],DB.TOGGLER[1],DB.TOGGLER[2],DB.TOGGLER[4],DB.TOGGLER[5] = WindowGetAnchor("ActionBarLockToggler",1) 
      DB.FLASHER[3],DB.FLASHER[1],DB.FLASHER[2],DB.FLASHER[4],DB.FLASHER[5] = WindowGetAnchor(WANCH,1)
   end
end
---------------------------------------------------------------------------------------------------------------------
local function add_hooks() 
   d("!> ADD HOOKS")
   ActionBar.ShowCaps   = function(...) end
   ActionBar.CreateCaps = function(...) end
   ActionButtonCreateOld = ActionButton.Create
   ActionButton.Create = function(...)
      local self = ActionButtonCreateOld(...)
      if (not self) then return nil end
      LabelSetFont(self.m_Windows[3].m_Name,DB.TIMER_FONT,22)
      LabelSetFont(self.m_Windows[1].m_Name,DB.LABEL_FONT,22)
      return self
   end

  --------------------------------------------------------------------------------------------------
    ActionButtonUpdateEnabledStateOld = ActionButton.UpdateEnabledState
    ActionButton.UpdateEnabledState = function(self, ...)
      ActionButtonUpdateEnabledStateOld(self, ...)
      if(DB.CD_LOS) then
         local icon = self.m_Windows[0]
         if (self.m_IsEnabled) and (self.m_IsTargetValid) then icon:SetTintColor(255,255,255)
         elseif (not self.m_IsEnabled) then icon:SetTintColor(255,20,0)
         elseif (not self.m_IsTargetValid) then icon:SetTintColor(255,20,0)
         elseif (not self.m_IsEnabled) and (iconType == 42) then icon:SetTintColor(150,150,150)
         else icon:SetTintColor(255, 255, 255)
         end 
      end
    end
   --------------------------------------------------------------------------------------------------
   ActionButtonUpdateCooldownAnimationOld = ActionButton.UpdateCooldownAnimation

if(not DB.CD_CLASSIC) then  
   ActionButton.UpdateCooldownAnimation = function(self,elapsed,cd_update)
      local cd_frame    = self.m_Windows[2]
      local timer       = self.m_Windows[3]
      local old_time    = self.m_Cooldown
      local flash       = false 
      self.m_Windows[2]:SetAlpha(DB.CD_ALPHA)
      if(cd_update) then
         local old_cd = self.m_Cooldown
         self.m_Cooldown, self.m_MaxCooldown = GetHotbarCooldown(self:GetSlot())
         if (old_cd and math.abs (old_cd - self.m_Cooldown) > ActionButton.IGNORE_COOLDOWN_DELTA_THRESHOLD) then
            cd_frame:Show (true)
            CooldownDisplaySetCooldown(cd_frame:GetName(),self.m_Cooldown,self.m_MaxCooldown)
            timer:Show(self.m_MaxCooldown > ActionButton.GLOBAL_COOLDOWN)
         end
      end
      self.m_Cooldown = self.m_Cooldown - elapsed
      if(self.m_Cooldown < 0) then flash = true end
      
      if(self.m_Cooldown > 0) then 
         if(self.m_MaxCooldown > ActionButton.GLOBAL_COOLDOWN) then 
            LabelSetFont(timer.m_Name,DB.TIMER_FONT,22)
            if(self.m_Cooldown < old_time) then
               if (self.m_Cooldown > 60) then 
                  timer:SetTextColor(0,100,255) 
                  timer:SetText(wstring.format(L"%d",self.m_Cooldown/60))
               elseif (self.m_Cooldown > 4) then 
                  timer:SetTextColor(100,255,0) 
                  timer:SetText(wstring.format(L"%d",self.m_Cooldown))
               else 
                  LabelSetFont(timer.m_Name,DB.TIMER_FONT_BIG,22)
                  timer:SetTextColor(255,0,0)
                  timer:SetText(wstring.format(L"%d",self.m_Cooldown))
                  if(DB.CDP_ENABLE and self.m_MaxCooldown > DB.CDP_TRASHOLD) then
                     if (self.m_Cooldown < 1) then 
                        snt_buttons.do_pulse(self.m_IconNum,true,self.m_Cooldown,self) 
                     end
                  end
               end
            end
         end
      elseif(self.m_Cooldown < 0 and cd_frame:IsShowing ()) then
         snt_buttons.do_pulse(self.m_IconNum,false,self.m_Cooldown,self)
         if(flash == true and DB.GCD_ENABLE) then self.m_Windows[4]:StartAnimation (0,false,true,0) end
         cd_frame:Show(false)
         self.m_Cooldown     = 0
         self.m_MaxCooldown  = 0
      end
   end
end   
   --------------------------------------------------------------------------------------------------
   if(not DB.CD_FLAME) then
      ActionButton.UpdateBurning = function(...) end
   end
   --------------------------------------------------------------------------------------------------
   if(DB.HIDE_TT) then
      ActionButton.OnMouseOver = function(...) end
   end

end
----------------------------------------------------------------------------------------------------------------------
-- ENTRY POINT
----------------------------------------------------------------------------------------------------------------------
function snt_buttons.on_load()
   if(first_load) then 
      local ok = false
      for _,val in pairs(LayoutEditor.EventHandlers ) do
         if(tostring(val) == tostring(snt_buttons.save_position)) then ok = true end
      end
      if(not ok) then table.insert(LayoutEditor.EventHandlers,snt_buttons.save_position) end
      --chat("snt buttons v "..VERSION.." type /sntbtn for settings")
      CreateWindow(WANCH,true) WindowSetDimensions(WANCH,DB.FLASH_SIZE,DB.FLASH_SIZE) WindowAddAnchor(WANCH,unpack(DB.FLASHER))
      WindowClearAnchors("ActionBarLockToggler") WindowAddAnchor("ActionBarLockToggler",unpack(DB.TOGGLER))
      LayoutEditor.RegisterWindow("ActionBarLockToggler",L"ActionBarToggler",L"ActionBarToggler",false,false,true,nil)
      LayoutEditor.RegisterWindow(WANCH,L"CD_Pulse",L"CD Pulse placeholder",true,true,true,nil)
      LibSlash.RegisterSlashCmd("sntbtn",snt_buttons.toggle_setup)
	  first_load = false
      snt_buttons.texture_buttons()
      d("!>LOADED")
   end
end

function snt_buttons.entry_point()
   d("!> ENTRY")
   if((not SNTBTN_DB) or (SNTBTN_DB.VERSION ~= VERSION)) then SNTBTN_DB = default end DB = SNTBTN_DB
   RegisterEventHandler(SystemData.Events.LOADING_END, "snt_buttons.on_load") -- doesn't work with WAR 1.2.1
   RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE,"snt_buttons.on_load") -- doesn't work with WAR 1.2.1
   --RegisterEventHandler(SystemData.Events.PLAYER_ZONE_CHANGED, "snt_buttons.on_load") fix for flight master, instances and other zoning - not needed, PLAYER_HOT_BAR_UPDATED will do
   RegisterEventHandler(SystemData.Events.PLAYER_HOT_BAR_UPDATED, "snt_buttons.texture_buttons") -- fix for draging skills and items into/from action bar
   
   add_hooks()
   actionbars_setup()
end       
----------------------------------------------------------------------------------------------------------------------
-- GUI SETUP CRAP
----------------------------------------------------------------------------------------------------------------------
local WSETUP = "SNT_BUTTONS_SETUP"

local function update_gui(need_reload)
   for ndx = 1,4 do
      local selector = L"H"
      if(DB.BAR[ndx].page == 42) then selector = L"<-" end
      if(DB.BAR[ndx].page == 43) then selector = L"->" end
      LabelSetText(WSETUP..ndx.."1LAB",towstring(DB.BAR[ndx].buttons)) 
      LabelSetText(WSETUP..ndx.."2LAB",towstring(DB.BAR[ndx].columns)) 
      LabelSetText(WSETUP..ndx.."3LAB",towstring(DB.BAR[ndx].preset)) 
      LabelSetText(WSETUP..ndx.."4LAB",selector) 
      ButtonSetPressedFlag(WSETUP.."C"..ndx.."1",DB.BAR[ndx].empty) 
      ButtonSetPressedFlag(WSETUP.."C"..ndx.."2",DB.BAR[ndx].back) 
      TextEditBoxSetText(WSETUP.."T"..ndx.."1",towstring(DB.BAR[ndx].x))
      TextEditBoxSetText(WSETUP.."T"..ndx.."2",towstring(DB.BAR[ndx].y))
   end  
   SliderBarSetCurrentPosition(WSETUP.."CDA",DB.CD_ALPHA)
   ButtonSetPressedFlag(WSETUP.."CD1",DB.GCD_ENABLE)
   ButtonSetPressedFlag(WSETUP.."CD2",DB.CD_FLAME)
   ButtonSetPressedFlag(WSETUP.."CD3",DB.CD_LOS)
   ButtonSetPressedFlag(WSETUP.."CD4",DB.HIDE_TT)
   ButtonSetPressedFlag(WSETUP.."CD5",DB.CD_CLASSIC)
   ButtonSetPressedFlag(WSETUP.."CP1",DB.CDP_ENABLE)
   ButtonSetPressedFlag(WSETUP.."CP2",DB.CDP_ALPHA)
   ButtonSetPressedFlag(WSETUP.."CP3",DB.CDP_ZOOM)
   ButtonSetPressedFlag(WSETUP.."CP4",DB.CDP_TEXT)
   ButtonSetPressedFlag(WSETUP.."CP5",DB.CDP_ICON)
   TextEditBoxSetText(WSETUP.."CPT1",towstring(DB.FLASH_SIZE))
   TextEditBoxSetText(WSETUP.."CPT2",towstring(DB.CDP_TRASHOLD))
   LabelSetText(WSETUP.."CPLAB",towstring(DB.CDP_SKIN)) 
   if(need_reload) then
      ButtonSetText(WSETUP.."_reload", L"NeedReload")
      WindowSetShowing(WSETUP.."_reload",true)
   else
      snt_buttons.texture_buttons()
   end
end
----------------------------------------------------------------------------------------------------------------------
function snt_buttons.slider_change() DB.CD_ALPHA  = SliderBarGetCurrentPosition(WSETUP.."CDA") end
----------------------------------------------------------------------------------------------------------------------
function snt_buttons.text_input() 
   DB.FLASH_SIZE     = tonumber(TextEditBoxGetText(WSETUP.."CPT1")) 
   DB.CDP_TRASHOLD   = tonumber(TextEditBoxGetText(WSETUP.."CPT2"))
    for ndx = 1,4 do
      DB.BAR[ndx].x = tonumber(TextEditBoxGetText(WSETUP.."T"..ndx.."1"))
      DB.BAR[ndx].y = tonumber(TextEditBoxGetText(WSETUP.."T"..ndx.."2"))
   end
   update_gui(true) 
end
----------------------------------------------------------------------------------------------------------------------
function snt_buttons.toggle_setup()
   if(not DoesWindowExist(WSETUP)) then
      CreateWindow(WSETUP,false)
      ButtonSetText(WSETUP.."_close", L"Close")
   end
   WindowSetShowing(WSETUP,not WindowGetShowing(WSETUP))    
   WindowSetShowing(WSETUP.."_reload",false)
   update_gui(false)
end
----------------------------------------------------------------------------------------------------------------------
local function inc_columns(ndx) 
   DB.BAR[ndx].columns = DB.BAR[ndx].columns + 1 
   if(DB.BAR[ndx].columns > DB.BAR[ndx].buttons) then DB.BAR[ndx].columns = 1 end
   update_gui(true)
end
----------------------------------------------------------------------------------------------------------------------
local function dec_columns(ndx) 
   DB.BAR[ndx].columns = DB.BAR[ndx].columns - 1 
   if(DB.BAR[ndx].columns < 1) then DB.BAR[ndx].columns = DB.BAR[ndx].buttons end
   update_gui(true)
end
----------------------------------------------------------------------------------------------------------------------
local function inc_buttons(ndx) 
   DB.BAR[ndx].buttons = DB.BAR[ndx].buttons + 1 
   if(DB.BAR[ndx].buttons > MAX_BUTTONS) then DB.BAR[ndx].buttons = 1 end
   update_gui(true)
end
----------------------------------------------------------------------------------------------------------------------
local function dec_buttons(ndx) 
   DB.BAR[ndx].buttons = DB.BAR[ndx].buttons - 1 
   if(DB.BAR[ndx].buttons < 1) then DB.BAR[ndx].buttons = MAX_BUTTONS end
   update_gui(true)
end
----------------------------------------------------------------------------------------------------------------------
local function inc_selector(ndx) 
   if(DB.BAR[ndx].page == 44) then DB.BAR[ndx].page = 42   
   else DB.BAR[ndx].page = DB.BAR[ndx].page + 1 end
   update_gui(true)
end
----------------------------------------------------------------------------------------------------------------------
local function dec_selector(ndx) 
   if(DB.BAR[ndx].page == 42) then DB.BAR[ndx].page = 44   
   else DB.BAR[ndx].page = DB.BAR[ndx].page - 1 end
   update_gui(true)
end
----------------------------------------------------------------------------------------------------------------------
local function inc_presets(ndx) 
   if(DB.BAR[ndx].preset == 16) then DB.BAR[ndx].preset = 1  
   else DB.BAR[ndx].preset = DB.BAR[ndx].preset + 1 end
   update_gui(false)
end
----------------------------------------------------------------------------------------------------------------------
local function dec_presets(ndx) 
   if(DB.BAR[ndx].preset == 1) then DB.BAR[ndx].preset = 16   
   else DB.BAR[ndx].preset = DB.BAR[ndx].preset - 1 end
   update_gui(false)
end
----------------------------------------------------------------------------------------------------------------------

function snt_buttons.CPL() if(DB.CDP_SKIN > 1) then DB.CDP_SKIN = DB.CDP_SKIN-1 end update_gui(false) end 
function snt_buttons.CPR() if(DB.CDP_SKIN < 8) then DB.CDP_SKIN = DB.CDP_SKIN+1 end update_gui(false) end 

function snt_buttons.B11L() dec_buttons(1) end function snt_buttons.B11R() inc_buttons(1) end
function snt_buttons.B12L() dec_columns(1) end function snt_buttons.B12R() inc_columns(1) end
function snt_buttons.B13L() dec_presets(1) end function snt_buttons.B13R() inc_presets(1) end
function snt_buttons.B14L() dec_selector(1)end function snt_buttons.B14R() inc_selector(1)end
function snt_buttons.B21L() dec_buttons(2) end function snt_buttons.B21R() inc_buttons(2) end
function snt_buttons.B22L() dec_columns(2) end function snt_buttons.B22R() inc_columns(2) end
function snt_buttons.B23L() dec_presets(2) end function snt_buttons.B23R() inc_presets(2) end
function snt_buttons.B24L() dec_selector(2)end function snt_buttons.B24R() inc_selector(2)end
function snt_buttons.B31L() dec_buttons(3) end function snt_buttons.B31R() inc_buttons(3) end
function snt_buttons.B32L() dec_columns(3) end function snt_buttons.B32R() inc_columns(3) end
function snt_buttons.B33L() dec_presets(3) end function snt_buttons.B33R() inc_presets(3) end
function snt_buttons.B34L() dec_selector(3)end function snt_buttons.B34R() inc_selector(3)end
function snt_buttons.B41L() dec_buttons(4) end function snt_buttons.B41R() inc_buttons(4) end
function snt_buttons.B42L() dec_columns(4) end function snt_buttons.B42R() inc_columns(4) end
function snt_buttons.B43L() dec_presets(4) end function snt_buttons.B43R() inc_presets(4) end
function snt_buttons.B44L() dec_selector(4)end function snt_buttons.B44R() inc_selector(4)end
function snt_buttons.C11() DB.BAR[1].empty = not DB.BAR[1].empty update_gui(true) end  
function snt_buttons.C12() DB.BAR[1].back  = not DB.BAR[1].back  update_gui(true) end  
function snt_buttons.C21() DB.BAR[2].empty = not DB.BAR[2].empty update_gui(true) end  
function snt_buttons.C22() DB.BAR[2].back  = not DB.BAR[2].back  update_gui(true) end  
function snt_buttons.C31() DB.BAR[3].empty = not DB.BAR[3].empty update_gui(true) end  
function snt_buttons.C32() DB.BAR[3].back  = not DB.BAR[3].back  update_gui(true) end  
function snt_buttons.C41() DB.BAR[4].empty = not DB.BAR[4].empty update_gui(true) end  
function snt_buttons.C42() DB.BAR[4].back  = not DB.BAR[4].back  update_gui(true) end  

function snt_buttons.CD1() DB.GCD_ENABLE  = not DB.GCD_ENABLE  update_gui(false) end  
function snt_buttons.CD2() DB.CD_FLAME    = not DB.CD_FLAME    update_gui(true)  end  
function snt_buttons.CD3() DB.CD_LOS      = not DB.CD_LOS      update_gui(true)  end  
function snt_buttons.CD4() DB.HIDE_TT     = not DB.HIDE_TT     update_gui(true)  end  
function snt_buttons.CD5() DB.CD_CLASSIC  = not DB.CD_CLASSIC  update_gui(true)  end  
function snt_buttons.CP1() DB.CDP_ENABLE  = not DB.CDP_ENABLE  update_gui(false) end  
function snt_buttons.CP2() DB.CDP_ALPHA   = not DB.CDP_ALPHA   update_gui(false) end  
function snt_buttons.CP3() DB.CDP_ZOOM    = not DB.CDP_ZOOM    update_gui(false) end  
function snt_buttons.CP4() DB.CDP_TEXT    = not DB.CDP_TEXT    update_gui(false) end  
function snt_buttons.CP5() DB.CDP_ICON    = not DB.CDP_ICON    update_gui(false) end  
----------------------------------------------------------------------------------------------------------------------




